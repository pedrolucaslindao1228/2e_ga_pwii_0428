import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL = this.URL_G

  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsAutores(): Observable<any> {
    return this.http.get<any>(`${this.URL}autores`)
  }

  buscarAutoresPorNome(nome: string): Observable<any> {
    return this.http.get<any>(`${this.URL}autores/pornome/${nome}`)
  }

  buscarAutoresPorSobrenome(sobrenome: string): Observable<any> {
    return this.http.get<any>(`${this.URL}autores/porsobrenome/${sobrenome}`)
  }

  buscarAutoresPorNacionalidade(nacionalidade: string): Observable<any> {
    return this.http.get<any>(`${this.URL}autores/pornacionalidade/${nacionalidade}`)
  }

  cadastrarAutor(autor: Autor): Observable<any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }
}
