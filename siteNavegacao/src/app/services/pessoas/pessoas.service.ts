import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pessoa } from 'src/app/models/Pessoa';

@Injectable({
  providedIn: 'root'
})
export class PessoasService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us77.gitpod.io/"
  private readonly URL = this.URL_J

  constructor(
    private http: HttpClient
  ) { }

  buscarPessoas():Observable<any> {
    return this.http.get<any>(`${this.URL}pessoas`)
  }

  buscarPessoasPorTelefone(telefone: string): Observable<any>{
    return this.http.get<any>(`${this.URL}pessoas/portelefone/${telefone}`)
  }

  buscarPessoasPorNome(nome: string): Observable<any>{
    return this.http.get<any>(`${this.URL}pessoas/pornome/${nome}`)
  }
  
  cadastrarPessoa(pessoa: Pessoa): Observable<any>{
    return this.http.post(`${this.URL}pessoa`, pessoa)
  }
}
