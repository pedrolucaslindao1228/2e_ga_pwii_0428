import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from 'src/app/models/Funcionario';

@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL = this.URL_G

  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsFuncionarios(): Observable<any>{
    return this.http.get<any>(`${this.URL}funcionarios`)
  }

  buscarFuncionariosPorNome(nome: string): Observable<any>{
    return this.http.get<any>(`${this.URL}funcionarios/pornome/${nome}`)
  }

  buscarFuncionariosPorSalario(max: number, min: number): Observable<any>{
    return this.http.get<any>(`${this.URL}funcionarios/porsalario/valormin/${min}/valormax/${max}`)
  }

  buscarFuncionariosPorStatus(status: boolean): Observable<any>{
    return this.http.get<any>(`${this.URL}funcionarios/porstatus/${status}`)
  }

  cadastrarFuncionario(funcionario: Funcionario): Observable<any> {
    return this.http.post<any>(`${this.URL}funcionario`, funcionario)
  }
}