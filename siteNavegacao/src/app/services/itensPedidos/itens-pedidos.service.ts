import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemPedido } from 'src/app/models/ItemPedido';

@Injectable({
  providedIn: 'root'
})
export class ItensPedidosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us77.gitpod.io/"
  private readonly URL = this.URL_J

  constructor(
    private http: HttpClient
  ) { }

  buscarItensPedidos():Observable<any>{
    return this.http.get<any>(`${this.URL}itensPedidos`)
  }

  buscarItensPedidosPorPedido(idPedido : number): Observable<any>{
    return this.http.get<any>(`${this.URL}itensPedidos/pedidos/${idPedido}`)
  }
  
  cadastrarItemPedido(itemPedido: ItemPedido): Observable<any>{
    return this.http.post(`${this.URL}itemPedido`, itemPedido)
  }

}
