import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Endereco } from 'src/app/models/Endereco';

@Injectable({
  providedIn: 'root'
})
export class EnderecosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL = this.URL_G

  constructor(
    private http: HttpClient
  ) { }

  buscarTodosOsEnderecos(): Observable<any> {
    return this.http.get<any>(`${this.URL}enderecos`)
  }

  buscarEnderecosPorCidadeEUf(cidade: string, uf: string): Observable<any> {
    return this.http.get<any>(`${this.URL}enderecos/porcidade-uf/${cidade}/${uf}`)
  }

  buscarEnderecosPorUf(uf: string): Observable<any> {
    return this.http.get<any>(`${this.URL}enderecos/porestado/${uf}`)
  }

  cadastrarEndereco(endereco: Endereco): Observable<any> {
    return this.http.post<any>(`${this.URL}endereco`, endereco)
  }
}
