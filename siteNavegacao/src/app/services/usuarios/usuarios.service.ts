import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us77.gitpod.io/"
  private readonly URL = this.URL_J

  constructor(
    private http: HttpClient
  ) { }

  buscarUsuarios(): Observable<any>{
    return this.http.get<any>(`${this.URL}usuarios`)
  }
  
  buscarUsuariosPorNome(nome: string): Observable<any>{
    return this.http.get<any>(`${this.URL}usuarios/pornome/${nome}`)
  }

  buscarUsuariosPorStatus(status: boolean): Observable<any>{
    return this.http.get<any>(`${this.URL}usuarios/porstatus/${status}`)
  }

  buscarUsuarioPorId(id: number): Observable<any>{
    return this.http.get<any>(`${this.URL}usuarios/porid/${id}`)
  }
  
  cadastrarUsuario(usuario: Usuario): Observable<any>{
    return this.http.post(`${this.URL}usuario`, usuario)
  }

}
