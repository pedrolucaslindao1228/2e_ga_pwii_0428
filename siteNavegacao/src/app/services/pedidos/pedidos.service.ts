import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pedido } from 'src/app/models/Pedido';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us77.gitpod.io/"
  private readonly URL = this.URL_J

  constructor(
    private http: HttpClient
  ) { }

  buscarPedidos(): Observable<any>{
    return this.http.get<any>(`${this.URL_J}pedidos`)
  }

  buscarPedidosPorData(data: Date): Observable<any>{
    return this.http.get<any>(`${this.URL}pedidos/pordata/${data}`)
  }

  buscarPedidosPorDataEntrega(dataEntrega: Date): Observable<any>{
    return this.http.get<any>(`${this.URL}pedidos/pordataentrega/${dataEntrega}`)
  }

  cadastrarPedido(pedido: Pedido): Observable<any> {
    return this.http.post<any>(`${this.URL}pedido`, pedido)
  }

}
