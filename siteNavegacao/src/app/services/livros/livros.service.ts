import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Livro } from 'src/app/models/Livro';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-d0eu1j8fi1g.ws-us77.gitpod.io/"
  private readonly URL = this.URL_J

  constructor(
    private http: HttpClient
  ) { }

  buscarLivros(): Observable<any> {
    return this.http.get<any>(`${this.URL}livros`)
  }

  buscarLivrosPorAutor(autor: number): Observable<any> {
    return this.http.get<any>(`${this.URL}livros/porautor/${autor}`)
  }

  buscarLivrosPorNome(titulo: string): Observable<any> {
    return this.http.get<any>(`${this.URL}livros/pornome/${titulo}`)
  }

  buscarLivrosPorEditora(editora: number): Observable<any> {
    return this.http.get<any>(`${this.URL}livros/poreditora/${editora}`)
  }

  cadastrarLivro(livro: Livro): Observable<any>{
    return this.http.post(`${this.URL}livro`, livro)
  }
}
