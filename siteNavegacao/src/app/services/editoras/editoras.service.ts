import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Editora } from 'src/app/models/Editora';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_J = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL_G = "https://3000-pedrolucasl-2egaapi0810-pt1byvr611j.ws-us78.gitpod.io/"
  private readonly URL = this.URL_G

  constructor(
    private http: HttpClient
  ) { }

  buscarTodasAsEditoras(): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras`)
  }

  buscarEditorasPorNome(nome: string): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras/pornome/${nome}`)
  }

  buscarEditorasPorRazaoSocial(razaosocial: string): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras/razaosocial/${razaosocial}`)
  }

  buscarEditorasPorCnpj(cnpj: string): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras/porcnpj/${cnpj}`)
  }

  cadastrarEditora(editora: Editora): Observable<any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }
}
