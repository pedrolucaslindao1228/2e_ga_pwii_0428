import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetalhesLivroComponent } from './components/detalhes-livro/detalhes-livro.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CadastrosComponent } from './components/cadastros/cadastros.component';
import { LivrosService } from './services/livros/livros.service';
import { PedidosService } from './services/pedidos/pedidos.service';
import { PessoasService } from './services/pessoas/pessoas.service';
import { UsuariosService } from './services/usuarios/usuarios.service';
import { ItensPedidosService } from './services/itensPedidos/itens-pedidos.service';
import { AutoresService } from './services/autores/autores.service';
import { EditorasService } from './services/editoras/editoras.service';
import { EnderecosService } from './services/enderecos/enderecos.service';
import { FuncionariosService } from './services/funcionarios/funcionarios.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DetalhesLivroComponent,
    PageNotFoundComponent,
    CadastrosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ 
    LivrosService,
    PedidosService,
    PessoasService,
    UsuariosService,
    ItensPedidosService,
    AutoresService,
    EditorasService,
    EnderecosService,
    FuncionariosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
