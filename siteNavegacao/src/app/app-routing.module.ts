import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home",
    loadChildren: () => import("./components/home/home.module").then(m => m.HomeModule)
  },
  {
    path: "login",
    loadChildren: () => import("./components/login/login.module").then(m => m.LoginModule)
  },
  {
    path: "detalheslivro",
    loadChildren: () => import("./components/detalhes-livro/detalhes-livro.module").then(m => m.DetalhesLivroModule)
  },
  {
    path: "cadastros",
    loadChildren: () => import("./components/cadastros/cadastros.module").then(m => m.CadastrosModule)
  },
  {
    path: "cadastro-autor",
    loadChildren: () => import("./components/admin/cad-autor/cad-autor.module").then(m => m.CadAutorModule)
  },
  {
    path: "cadastro-editora",
    loadChildren: () => import("./components/admin/cad-editora/cad-editora.module").then(m => m.CadEditoraModule)
  },
  {
    path: "cadastro-livro",
    loadChildren: () => import("./components/admin/cad-livro/cad-livro.module").then(m => m.CadLivroModule)
  },
  {
    path: "cadastro-pessoa",
    loadChildren: () => import("./components/admin/cad-pessoa/cad-pessoa.module").then(m => m.CadPessoaModule)
  },
  {
    path: "cadastro-pedido",
    loadChildren:() => import("./components/admin/cad-pedido/cad-pedido.module").then(m => m.CadPedidoModule)
  },
  {
    path: "cadastro-itemPedido",
    loadChildren:() => import("./components/admin/cad-item-pedido/cad-item-pedido.module").then(m => m.CadItemPedidoModule)
  },
  {
    path: "cadastro-usuario",
    loadChildren: () => import("./components/admin/cad-usuario/cad-usuario.module").then(m => m.CadUsuarioModule)
  },
  {
    path: "itemPedido",
    loadChildren: () => import("./serviceComponents/item-pedido/item-pedido.module").then(m => m.ItemPedidoModule)
  },
  {
    path: "livro",
    loadChildren: () => import("./serviceComponents/livro/livro.module").then(m => m.LivroModule)
  },
  {
    path: "pedido",
    loadChildren: () => import("./serviceComponents/pedido/pedido.module").then(m => m.PedidoModule)
  },
  {
    path: "pessoa",
    loadChildren: () => import("./serviceComponents/pessoa/pessoa.module").then(m => m.PessoaModule)
  },
  {
    path: "usuario",
    loadChildren: () => import("./serviceComponents/usuario/usuario.module").then(m => m.UsuarioModule)
  },
  {
    path: "autor",
    loadChildren: () => import("./serviceComponents/autor/autor.module").then(m => m.AutorModule)
  },
  {
    path: "editora",
    loadChildren: () => import("./serviceComponents/editora/editora.module").then(m => m.EditoraModule)
  },
  {
    path: "endereco",
    loadChildren: () => import("./serviceComponents/endereco/endereco.module").then(m => m.EnderecoModule)
  },
  {
    path: "funcionario",
    loadChildren: () => import("./serviceComponents/funcionario/funcionario.module").then(m => m.FuncionarioModule)
  },
  {
    path: "cadastro-funcionario",
    loadChildren: () => import("./components/admin/cad-funcionario/cad-funcionario.module").then(m => m.CadFuncionarioModule)
  },
  {
    path: "cadastro-endereco",
    loadChildren: () => import("./components/admin/cad-endereco/cad-endereco.module").then(m => m.CadEnderecoModule)
  },
  {
    path: "**",
    loadChildren: () => import("./components/page-not-found/page-not-found.module").then(m => m.PageNotFoundModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
