export class Endereco{
    id: number = 0
    id_pessoa: number = 0
    descricao: string = ""
    rua: string = ""
    numero: string = ""
    bairro: string = ""
    cep: string = ""
    cidade: string = ""
    uf: string = ""
    complemento: string = ""
}