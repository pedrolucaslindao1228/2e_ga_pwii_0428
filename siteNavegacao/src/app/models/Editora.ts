import { Pessoa } from './Pessoa'

export class Editora extends Pessoa{
    razao_social: string = ""
    cnpj: string = ""
    ie: string = ""
}

/*export class Editora{
    private _nome: string;
    private _endereco: string;
    private _bairro: string;
    private _cep: string;
    private _cidade: string;
    private _uf: string;

        constructor(nome:string, endereco:string, bairro:string, cep:string, cidade:string, uf:string){
            this._nome = nome;
            this._endereco = endereco;
            this._bairro = bairro;
            this._cep = cep;
            this._cidade = cidade;
            this._uf = uf;
        }
    
    public set nome(nome: string){
        this._nome = nome;
    }

    public get nome(): string{
        return this._nome;
    }

    public set endereco(endereco: string){
        this._endereco = endereco;
    }

    public get endereco(): string{
        return this._endereco
    }

    public set bairro(bairro: string){
        this._bairro = bairro;
    }

    public get bairro(): string{
        return this._bairro;
    }

    public set cep(cep: string){
        this._cep = cep;
    }
    
    public get cep(): string{
        return this._cep;
    }

    public set cidade(cidade: string){
        this._cidade = cidade;
    }

    public get cidade(): string{
        return this._cidade;
    }

    public set uf(uf: string){
        this._uf = uf;
    }
    
    public get uf(): string{
        return this._uf;
    }
}*/