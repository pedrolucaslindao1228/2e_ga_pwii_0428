export class Livro{
    id: number = 0 
    titulo: string = "" 
    paginas: number = 0
    resumo: string = "" 
    preco: number = 0 
    idioma: string = "" 
    edicao: number = 0 
    descricao: string = "" 
    id_autor: number = 0 
    id_editora: number = 0 
    data_publicacao: Date = new Date()
}

/*export class Livro{
    private _titulo: string;
    private _paginas: Number;
    private _idioma: string;
    private _anopublicacao: string;
    private _edicao: Number;
    private _preco: Number;

    constructor(titulo:string,paginas:Number, idioma:string, anopublicacao:string, edicao:number, preco:number){
        this._titulo = titulo;
        this._paginas = paginas;
        this._idioma = idioma;
        this._anopublicacao = anopublicacao;
        this._edicao = edicao;
        this._preco = preco;
    }

    public set titulo(titulo: string){
        this._titulo = titulo;
    }

    public get titulo():string{
        return this._titulo;
    }

    public set paginas(paginas:Number){
        this._paginas = paginas;
    }

    public get paginas():Number{
        return this._paginas;
    }

    public set idioma(idioma:string){
        this._idioma = idioma;
    }

    public get idioma(): string{
        return this._idioma;
    }

    public set anopublicacao(anopublicacao:string){
        this._anopublicacao = anopublicacao;
    }

    public get anopublicacao():string{
        return this._anopublicacao;
    }

    public set edicao(edicao:Number){
        this._edicao = edicao;
    }

    public get edicao():Number{
        return this._edicao;
    }

    public set preco(preco:Number){
        this._preco = preco;
    }

    public get preco():Number{
        return this._preco;
    }
}*/