import { Usuario } from './Usuario';

export class Funcionario extends Usuario {
    override id: number = 0
    data_admissao: Date = this.data_cadastro
    salario: number = 0
    cargo: string = ""
}
