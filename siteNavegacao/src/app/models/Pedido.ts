import { Pessoa } from "./Pessoa";

export class Pedido extends Pessoa{
    id_cliente: number = 0
    data: Date = new Date() 
    previsao_entrega: Date = new Date()
    forma_pagamento: string = "" 
    observacoes: string = ""
}