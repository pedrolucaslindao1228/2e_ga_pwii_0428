import { Pessoa } from './Pessoa';

export class Autor extends Pessoa{
    nacionalidade: string = ""
    data_morte: Date = new Date()
}

/*export class Autor{
    private _nome: string;
    private _nacionalidade: string;
    private _sexo: string;
    private _dataNascimento: Date;

    constructor(nome:string, nacionalidade:string, sexo:string, dataNascimento:Date){
        this._nome = nome;
        this._dataNascimento = dataNascimento;
        this._nacionalidade = nacionalidade;
        this._sexo = sexo;
    }

    public set nome(nome: string){
        this._nome = nome;
    }
    public get nome(): string{
        return this._nome;
    }

    public set nacionalidade(nacionalidade: string){
        this._nacionalidade = nacionalidade;
    }

    public get nacionalidade(): string{
        return this._nacionalidade;
    }

    public set sexo(sexo: string){
        this._sexo = sexo;
    }

    public get sexo(): string{
        return this._sexo;
    }

    public set dataNascimento(dataNascimento: Date){
        this._dataNascimento = dataNascimento;
    }

    public get dataNascimento(): Date{
        return this._dataNascimento;
    }
}*/
