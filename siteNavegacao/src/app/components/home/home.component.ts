import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {



  categorias = [
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLU44FFdVVMxMIZ7bNNNT-OwbV5F5m7pVugdb-gBNedNU3nnkbkB1hXgirNKic03APZ6KzSLrg-o4F2pQ675CVYLPRBEThYtrhNA-IRdY83L01tDNHDW3ipgmQFqNS-KtSyz38pMuIuwwjj4SDtgg-iP=w600-h400-no?authuser=0",
      alt: "Romance",
      titulo: "Romance"
    },
    {
      src: "https://st3.depositphotos.com/3584689/14640/i/600/depositphotos_146407939-stock-photo-scenery-of-lonely-woman-looking.jpg",
      alt: "Ficção",
      titulo: "Ficção"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWMleKOVhCEniRGaQoMjOkl2Nwp0BD8bGx75GbyNExyijZ5IAQ74HD3dGfOgO6B6sPAnL1dfLFyiR809odfXqgE1D5_PcDRTnfdVMCcagJgmZwWo_OQnf1o-Lc-e75VXqX_qi3v-Pv9epFYq8fIZYA9=w600-h400-no?authuser=0",
      alt: "Arte",
      titulo: "Arte"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWF_wV7gl2z4JwuYcPtAGa75q8HZrR8-kw_jRAleA_40CEQbgJIJYCG15WCCeSyfafMOoVj8G5D-Ij1xurBNBmCD0uknZJRh5ChJNQqRt3GU2rVRuqMAB3BF-Odhhs3QAl4f3UvE_JxuBH7Owuy1YA6=w600-h400-no?authuser=0",
      alt: "Aventura",
      titulo: "Aventura"
    },
    {
      src: "https://static.mundoeducacao.uol.com.br/mundoeducacao/2019/08/biografia.jpg",
      alt: "Biografia",
      titulo: "Biografia"
    },
    {
      src: "https://static.portugues.com.br/2020/12/conto.jpg",
      alt: "Conto",
      titulo: "Conto"
    }
  ]

  livros = [
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWzzgkrEV0CGe93EK4EGGONyMl3vzS9HuoxT0_9tMaM4btMh9MDgs37IomMg-J4XE5sxarRda9RsUTTL_e3JjyktPIyVAivTmNzJ3SKgtJXPuOS1DAyBz-7qsdju1_0vAqzrFgA87s9msbSCc-S-owa=w549-h658-no?authuser=0",
      titulo: "Adicionar",
      rota: "/cadastros"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLX4woY_q03AeKZGYplkxbdTzgW39TUAe3b8uLcWkC0-Vc318otgQibL0BXGEXo8DcVOQn1nfKZ8tIZhxUE6MEIeY8eFmbE8SknKn57WUOJOBxi-_TXcEqE2Kn7X5TnOqus3HyRSDmbQLUDFAB1pr5CU=w549-h658-no?authuser=0",
      titulo: "As vantagens de ser invisível"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLUTWvGph1yEzACT2vutKhJ_wMFdwnN1yB0H09WOaKwgsojmr_Oi9AncSxmSKI8PjKpuKYMXC09rrjrj0J9dBQGIp6t8RXJzoPjCCVM9TRh6vhms9Ul4PYeVdVo6UGViAmNFgUlMhRn2Ne0G6C8aLAO-=w549-h658-no?authuser=0",
      titulo: "Amor gelato"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLXgmlDfr2cyb-sdc-ecMSjBE3z3SpMDudJEv439edPqTPDV1v40ogJ6V_IgYTXsWcqY-_kJUWY8rneE-eK1vN26Qv2X2DeGupGfRHk1RruF7VLdIzcmt4jMtXVJ1J8V7RgknEl6O_PlB_LyLc6eCeGF=w549-h658-no?authuser=0",
      titulo: "O milagre da manhã"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWywf4i4X7zjfr6aJsF0hvOHjULPRSpfA3pV9LO5L-hYtyj3_jhjs9hE_lBhyBga8JrJRzsFOGmEnwRnwC6aVIHAxD6nz0796tLrqdamX9CFvfv6kA5dv3sBvgUjJHbTFYY4RP41iC0VQNZ8wUiym4J=w549-h658-no?authuser=0",
      titulo: "Não se humilha, não"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLXQpEtuHIeAKCJW2gpX4dyd4M0go9Jm4GO0LB4PMwLMZKMAZ500VbXSrX2p8t4a9dATsNkt00yp0b72LVyKmd76R47YiLGCOgxVu9FrRTpdLxARLdHfIjj0LOu-WQ8IHfLtltA3t64WlbjUM1BnnsY_=w549-h658-no?authuser=0",
      titulo: "O conto da aia"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWaSa0zuNDLJfzaFlVyhGfggkkm1erXP7QEdoU1CnJ6t-GtzoesZSd8CnFuD4u2Xi1SGpytj2FThEWMtiyConft17sHUPYiKpmpIB0bYzZNpER4HX0kPpmYkCCl4cPG1UStwE8vytesElah9gLR0nnd=w549-h658-no?authuser=0",
      titulo: "Mindset"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLXC3KOV4wgJ5QP4HvijwMnft_Bwp_kVT83xqgbM6Su2czWjfwUwPtXnkBiaO41d29so5_OOYvJPsBVPEER9hxMlUtcsr8ZrgL4anPTR1jGkHF2yV819T7nyTnJLdP1TZgfvuFsTSB6W8TX2WEQY-k6d=w549-h658-no?authuser=0",
      titulo: "A garota do lago"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLW3vTFOHfkw-StGO-efsFPds3d2rzd4F9X56jAqYaxfvLOaXFbB0-ITtXNnQ3Bx2LewwgCneaHaraontOlZgkYVELvqJrWYzYxQkyI_o7ja67CgY4jDkfHcXyCt84cw0j4d69IAQ4ylDSgfnCEG5dcd=w549-h658-no?authuser=0",
      titulo: "Verity"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLWjwN_ef_vEoXoz39I-AkqV1EcbjrWW7t77pqu-La3ntHdFeatzX8tSXmJtyFX55ETOzXTxwV3EDDbVIbKVFrs0cmRX1-h0xqNhPHmBtOEym0DlgMm_Kw4KnUz13I_gAXVUxYgeZD3otl-vTBCjC5Ou=w549-h658-no?authuser=0",
      titulo: "Lovecraft 2"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLXGn84GXlCtA98EvbzCVPDOkws57nj8G539-2OCzLTVEbvxwxWgbOwvNQ6oeOpHANsZ9SShohLPYsJ19LYFIi2pHReLgrPGwrJR8vzNoqVr_SbgjpyklXvh2HsExec88UOMAIW7PxBAb21IKE9Wvhom=w549-h658-no?authuser=0",
      titulo: "O poder da autorresponsabilidade"
    },
    {
      src: "https://lh3.googleusercontent.com/pw/AM-JKLW9ab1hzYKibobuCugOME3z08_Rsm1aWVsD-eiTn8gsLY5eAZpQk5tbh9Em_3RHp1QzCpb3WAku2Zrj-YEvOSQB5VIkXK1egBHkcXzo4aN7AoYsjUZvZgXnU4JFVuMkqO1P77YO69JLTDnN9eET3KH6=w549-h658-no?authuser=0",
      titulo: "Ensaio sobre a cegueira"
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
