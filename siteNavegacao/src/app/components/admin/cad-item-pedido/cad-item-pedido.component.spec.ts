import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadItemPedidoComponent } from './cad-item-pedido.component';

describe('CadItemPedidoComponent', () => {
  let component: CadItemPedidoComponent;
  let fixture: ComponentFixture<CadItemPedidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadItemPedidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadItemPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
