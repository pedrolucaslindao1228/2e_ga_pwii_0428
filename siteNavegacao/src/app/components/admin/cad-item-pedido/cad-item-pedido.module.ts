import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadItemPedidoRoutingModule } from './cad-item-pedido-routing.module';
import { CadItemPedidoComponent } from './cad-item-pedido.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadItemPedidoComponent
  ],
  imports: [
    CommonModule,
    CadItemPedidoRoutingModule,
    FormsModule
  ]
})
export class CadItemPedidoModule { }
