import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadItemPedidoComponent } from './cad-item-pedido.component';

const routes: Routes = [
  {
    path: "",
    component: CadItemPedidoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadItemPedidoRoutingModule { }
