import { Component, OnInit } from '@angular/core';
import { ItemPedido } from 'src/app/models/ItemPedido';
import { ItensPedidosService } from 'src/app/services/itensPedidos/itens-pedidos.service';

@Component({
  selector: 'app-cad-item-pedido',
  templateUrl: './cad-item-pedido.component.html',
  styleUrls: ['./cad-item-pedido.component.scss']
})
export class CadItemPedidoComponent implements OnInit {
  itemPedido: ItemPedido

  constructor(
    private itensPedidosService: ItensPedidosService
  ) { 
    this.itemPedido = new ItemPedido()
  }

  ngOnInit(): void {
  }

  salvar(): void{
    this.itensPedidosService.cadastrarItemPedido(this.itemPedido).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }


}
