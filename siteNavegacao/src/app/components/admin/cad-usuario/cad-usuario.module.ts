import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadUsuarioRoutingModule } from './cad-usuario-routing.module';
import { CadUsuarioComponent } from './cad-usuario.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadUsuarioComponent
  ],
  imports: [
    CommonModule,
    CadUsuarioRoutingModule,
    FormsModule
  ]
})
export class CadUsuarioModule { }
