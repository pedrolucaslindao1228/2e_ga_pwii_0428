import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.component.html',
  styleUrls: ['./cad-usuario.component.scss']
})
export class CadUsuarioComponent implements OnInit {
  usuario: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) { 
    this.usuario = new Usuario()
  }

  ngOnInit(): void {
  }

  salvar(): void{
    this.usuariosService.cadastrarUsuario(this.usuario).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
