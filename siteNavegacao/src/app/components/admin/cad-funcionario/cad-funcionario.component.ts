import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-cad-funcionario',
  templateUrl: './cad-funcionario.component.html',
  styleUrls: ['./cad-funcionario.component.scss']
})
export class CadFuncionarioComponent implements OnInit {
  funcionario: Funcionario

  constructor(private funcionariosService: FuncionariosService) {
    this.funcionario = new Funcionario()
  }

  ngOnInit(): void {
  }

  cadastrarFuncionario(): void {
    this.funcionariosService.cadastrarFuncionario(this.funcionario).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
