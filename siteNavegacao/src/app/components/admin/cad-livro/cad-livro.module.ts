import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadLivroRoutingModule } from './cad-livro-routing.module';
import { CadLivroComponent } from './cad-livro.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadLivroComponent
  ],
  imports: [
    CommonModule,
    CadLivroRoutingModule,
    FormsModule
  ]
})
export class CadLivroModule { }
