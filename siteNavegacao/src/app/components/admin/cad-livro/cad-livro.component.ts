import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/models/Livro';
import { LivrosService } from 'src/app/services/livros/livros.service';

@Component({
  selector: 'app-cad-livro',
  templateUrl: './cad-livro.component.html',
  styleUrls: ['./cad-livro.component.scss']
})
export class CadLivroComponent implements OnInit {
  livro: Livro

  constructor(
    private livrosService: LivrosService
  ) { 
    this.livro = new Livro()
  }

  ngOnInit(): void {
  }

  salvar(): void{
    this.livrosService.cadastrarLivro(this.livro).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
