import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/models/Pessoa';
import { PessoasService } from 'src/app/services/pessoas/pessoas.service';

@Component({
  selector: 'app-cad-pessoa',
  templateUrl: './cad-pessoa.component.html',
  styleUrls: ['./cad-pessoa.component.scss']
})
export class CadPessoaComponent implements OnInit {
  pessoa: Pessoa

  constructor(
    private pessoasService: PessoasService
  ) {
    this.pessoa = new Pessoa()
   }

  ngOnInit(): void {
  }

  salvar(): void{
    this.pessoasService.cadastrarPessoa(this.pessoa).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
