import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadPessoaRoutingModule } from './cad-pessoa-routing.module';
import { CadPessoaComponent } from './cad-pessoa.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadPessoaComponent
  ],
  imports: [
    CommonModule,
    CadPessoaRoutingModule,
    FormsModule
  ]
})
export class CadPessoaModule { }
