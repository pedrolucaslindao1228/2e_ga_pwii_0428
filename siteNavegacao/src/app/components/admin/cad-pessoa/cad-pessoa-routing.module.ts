import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadPessoaComponent } from './cad-pessoa.component';

const routes: Routes = [
  {
    path: "",
    component: CadPessoaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadPessoaRoutingModule { }
