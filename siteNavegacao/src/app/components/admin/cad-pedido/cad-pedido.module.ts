import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadPedidoRoutingModule } from './cad-pedido-routing.module';
import { CadPedidoComponent } from './cad-pedido.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadPedidoComponent
  ],
  imports: [
    CommonModule,
    CadPedidoRoutingModule,
    FormsModule
  ]
})
export class CadPedidoModule { }
