import { Component, OnInit } from '@angular/core';
import { Pedido } from 'src/app/models/Pedido';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';

@Component({
  selector: 'app-cad-pedido',
  templateUrl: './cad-pedido.component.html',
  styleUrls: ['./cad-pedido.component.scss']
})
export class CadPedidoComponent implements OnInit {
  pedido: Pedido

  constructor(
    private pedidosService: PedidosService
  ) { 
    this.pedido = new Pedido()
  }

  ngOnInit(): void {
  }

  salvar(): void{
    this.pedidosService.cadastrarPedido(this.pedido).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
