import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadPedidoComponent } from './cad-pedido.component';

const routes: Routes = [
  {
    path: "",
    component: CadPedidoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadPedidoRoutingModule { }
