import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadAutorRoutingModule } from './cad-autor-routing.module';
import { CadAutorComponent } from './cad-autor.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadAutorComponent
  ],
  imports: [
    CommonModule,
    CadAutorRoutingModule,
    FormsModule
  ]
})
export class CadAutorModule { }
