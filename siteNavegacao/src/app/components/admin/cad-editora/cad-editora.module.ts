import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadEditoraRoutingModule } from './cad-editora-routing.module';
import { CadEditoraComponent } from './cad-editora.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadEditoraComponent
  ],
  imports: [
    CommonModule,
    CadEditoraRoutingModule,
    FormsModule
  ]
})
export class CadEditoraModule { }
