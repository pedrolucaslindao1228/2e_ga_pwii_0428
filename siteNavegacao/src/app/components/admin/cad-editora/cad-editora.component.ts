import { Component, OnInit } from '@angular/core';
import { Editora } from 'src/app/models/Editora';
import { EditorasService } from 'src/app/services/editoras/editoras.service';

@Component({
  selector: 'app-cad-editora',
  templateUrl: './cad-editora.component.html',
  styleUrls: ['./cad-editora.component.scss']
})
export class CadEditoraComponent implements OnInit {
  editora: Editora

  constructor(private editorasService: EditorasService) {
    this.editora = new Editora()
  }

  ngOnInit(): void {
  }

  cadastrarEditora(): void {
    this.editorasService.cadastrarEditora(this.editora).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error (erro)
      }
    })
  }

}
