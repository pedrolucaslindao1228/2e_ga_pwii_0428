import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadEnderecoComponent } from './cad-endereco.component';

const routes: Routes = [
  {
    path: "",
    component: CadEnderecoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadEnderecoRoutingModule { }
