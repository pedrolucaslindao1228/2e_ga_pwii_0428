import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadEnderecoRoutingModule } from './cad-endereco-routing.module';
import { CadEnderecoComponent } from './cad-endereco.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CadEnderecoComponent
  ],
  imports: [
    CommonModule,
    CadEnderecoRoutingModule,
    FormsModule
  ]
})
export class CadEnderecoModule { }
