import { Component, OnInit } from '@angular/core';
import { Endereco } from 'src/app/models/Endereco';
import { EnderecosService } from 'src/app/services/enderecos/enderecos.service';

@Component({
  selector: 'app-cad-endereco',
  templateUrl: './cad-endereco.component.html',
  styleUrls: ['./cad-endereco.component.scss']
})
export class CadEnderecoComponent implements OnInit {
  endereco: Endereco
  constructor(private enderecosService: EnderecosService) {
    this.endereco = new Endereco()
  }

  ngOnInit(): void {
  }

  cadastrarEndereco(): void {
    this.enderecosService.cadastrarEndereco(this.endereco).subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error (erro)
      }
    })
  }
}
