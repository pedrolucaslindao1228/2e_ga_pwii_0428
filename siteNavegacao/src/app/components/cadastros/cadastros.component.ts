import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastros',
  templateUrl: './cadastros.component.html',
  styleUrls: ['./cadastros.component.scss']
})
export class CadastrosComponent implements OnInit {

  buttons = [
    {
      texto: "Cadastrar Autor",
      rota: "/cadastro-autor"
    },
    {
      texto: "Cadastrar Editora",
      rota: "/cadastro-editora"
    },
    {
      texto: "Cadastrar Livro",
      rota: "/cadastro-livro"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
