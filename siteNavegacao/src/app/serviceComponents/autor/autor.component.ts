import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { AutoresService } from 'src/app/services/autores/autores.service';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.scss']
})
export class AutorComponent implements OnInit {
  autores: Autor[]

  constructor(
    private autoresService: AutoresService
  ) { 
    this.autores = []
  }

  ngOnInit(): void {
    this.autoresService.buscarTodosOsAutores().subscribe({
      next: (resposta) => {
        this.autores = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
