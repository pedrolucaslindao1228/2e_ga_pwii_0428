import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemPedidoRoutingModule } from './item-pedido-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ItemPedidoRoutingModule
  ]
})
export class ItemPedidoModule { }
