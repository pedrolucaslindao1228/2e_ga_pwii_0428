import { Component, OnInit } from '@angular/core';
import { ItemPedido } from 'src/app/models/ItemPedido';
import { ItensPedidosService } from 'src/app/services/itensPedidos/itens-pedidos.service';

@Component({
  selector: 'app-item-pedido',
  templateUrl: './item-pedido.component.html',
  styleUrls: ['./item-pedido.component.scss']
})
export class ItemPedidoComponent implements OnInit {
  itensPedidos: ItemPedido[]
  constructor(
    private itensPedidosService: ItensPedidosService
  ) { 
    this.itensPedidos = []
  }

  ngOnInit(): void {
    this.itensPedidosService.buscarItensPedidos().subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })

    this.itensPedidosService.buscarItensPedidosPorPedido(1).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
