import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {
  usuarios: Usuario[]

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.usuarios = []
   }

  ngOnInit(): void {
    this.usuariosService.buscarUsuarios().subscribe({
      next: (resposta) => {
        this.usuarios = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.usuariosService.buscarUsuariosPorNome("joao").subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })

    this.usuariosService.buscarUsuariosPorStatus(true).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })

    this.usuariosService.buscarUsuarioPorId(4).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
