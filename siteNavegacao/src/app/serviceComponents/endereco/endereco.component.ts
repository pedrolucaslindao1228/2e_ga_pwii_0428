import { Component, OnInit } from '@angular/core';
import { Endereco } from 'src/app/models/Endereco';
import { EnderecosService } from 'src/app/services/enderecos/enderecos.service';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.scss']
})
export class EnderecoComponent implements OnInit {
  enderecos: Endereco[]

  constructor(
    private enderecosService: EnderecosService
  ) {
    this.enderecos = []
  }

  ngOnInit(): void {
    this.enderecosService.buscarTodosOsEnderecos().subscribe({
      next: (resposta) => {
        this.enderecos = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}