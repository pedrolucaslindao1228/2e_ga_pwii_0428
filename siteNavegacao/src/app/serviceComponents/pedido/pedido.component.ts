import { Component, OnInit } from '@angular/core';
import { Pedido } from 'src/app/models/Pedido';
import { PedidosService } from 'src/app/services/pedidos/pedidos.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent implements OnInit {
  pedidos: Pedido[]

  constructor(
    private pedidosService: PedidosService
  ) {
    this.pedidos = []
   }

  ngOnInit(): void {
    this.pedidosService.buscarPedidos().subscribe({
      next: (resposta) => {
        this.pedidos = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.pedidosService.buscarPedidosPorData(new Date(2021-10-23)).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.pedidosService.buscarPedidosPorDataEntrega(new Date(2022-7-1)).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
