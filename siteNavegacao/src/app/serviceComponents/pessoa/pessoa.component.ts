import { Component, OnInit } from '@angular/core';
import { Pessoa } from 'src/app/models/Pessoa';
import { PessoasService } from 'src/app/services/pessoas/pessoas.service';

@Component({
  selector: 'app-pessoa',
  templateUrl: './pessoa.component.html',
  styleUrls: ['./pessoa.component.scss']
})
export class PessoaComponent implements OnInit {
  pessoas: Pessoa[]

  constructor(
    private pessoasService: PessoasService
  ) {
    this.pessoas = []
  }

  ngOnInit(): void {
    this.pessoasService.buscarPessoas().subscribe({
      next: (resposta) => {
        this.pessoas = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
    
    this.pessoasService.buscarPessoasPorNome("joao").subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.pessoasService.buscarPessoasPorTelefone("24444-4444").subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }

}
