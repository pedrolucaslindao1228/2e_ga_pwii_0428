import { Component, OnInit } from '@angular/core';
import { Funcionario } from 'src/app/models/Funcionario';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';

@Component({
  selector: 'app-funcionario',
  templateUrl: './funcionario.component.html',
  styleUrls: ['./funcionario.component.scss']
})
export class FuncionarioComponent implements OnInit {
  funcionarios: Funcionario[]
  constructor(
    private funcionariosService: FuncionariosService
  ) {
    this.funcionarios = []
  }

  ngOnInit(): void {
    this.funcionariosService.buscarTodosOsFuncionarios().subscribe({
      next: (resposta) => {
        this.funcionarios = resposta.results
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
