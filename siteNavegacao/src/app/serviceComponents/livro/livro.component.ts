import { Component, OnInit } from '@angular/core';
import { Livro } from 'src/app/models/Livro';
import { LivrosService } from 'src/app/services/livros/livros.service';

@Component({
  selector: 'app-livro',
  templateUrl: './livro.component.html',
  styleUrls: ['./livro.component.scss']
})
export class LivroComponent implements OnInit {
  livros: Livro[]

  constructor(
    private livrosService: LivrosService
  ) { 
    this.livros = []
  }

  ngOnInit(): void {
    this.livrosService.buscarLivros().subscribe({
      next: (resposta) => {
        this.livros = resposta.results
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.livrosService.buscarLivrosPorAutor(5).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.livrosService.buscarLivrosPorNome("Os").subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })

    this.livrosService.buscarLivrosPorEditora(5).subscribe({
      next: (resposta) => {
        console.log(resposta)
      },
      error: (erro) =>{
        console.error(erro)
      }
    })
  }

}
