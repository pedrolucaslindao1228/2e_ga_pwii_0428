import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditoraRoutingModule } from './editora-routing.module';
import { EditoraComponent } from './editora.component';


@NgModule({
  declarations: [
    EditoraComponent
  ],
  imports: [
    CommonModule,
    EditoraRoutingModule
  ]
})
export class EditoraModule { }
