import { Component, OnInit } from '@angular/core';
import { Editora } from 'src/app/models/Editora';
import { EditorasService } from 'src/app/services/editoras/editoras.service';

@Component({
  selector: 'app-editora',
  templateUrl: './editora.component.html',
  styleUrls: ['./editora.component.scss']
})
export class EditoraComponent implements OnInit {
  editoras: Editora[]

  constructor(
    private editorasService: EditorasService
  ) {
    this.editoras = []
  }

  ngOnInit(): void {
    this.editorasService.buscarTodasAsEditoras().subscribe({
      next: (resposta) => {
        this.editoras = resposta.results
      },
      error: (erro) => {
        console.error (erro)
      }
    })
  }

}
