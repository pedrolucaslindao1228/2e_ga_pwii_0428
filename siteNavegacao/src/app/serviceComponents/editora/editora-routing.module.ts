import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditoraComponent } from './editora.component';

const routes: Routes = [
  {
    path: "",
    component: EditoraComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditoraRoutingModule { }
